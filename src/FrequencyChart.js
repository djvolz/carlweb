import React, { Component } from 'react';
import { Sparklines, SparklinesLine, SparklinesSpots } from 'react-sparklines';
// import { Event } from 'react-socket-io';


// Import Style
// import styles from './Footer.css';


function boxMullerRandom () {
    let phase = false,
        x1, x2, w;

    return (function() {

        phase = !phase
        if (phase) {
            do {
                x1 = 2.0 * Math.random() - 1.0;
                x2 = 2.0 * Math.random() - 1.0;
                w = x1 * x1 + x2 * x2;
            } while (w >= 1.0);

            w = Math.sqrt((-2.0 * Math.log(w)) / w);
            return x1 * w;
        } else {
            return x2 * w;
        }
    })();
}

// function randomData(n = 30) {
//     return Array.apply(0, Array(n)).map(boxMullerRandom);
// }
// const sampleData = randomData(30);
// const sampleData100 = randomData(100);

class Dynamic extends Component {

    constructor(props) {
        super(props);
        this.state = { data: [] };
        setInterval(() =>
            this.setState({
                data: this.state.data.concat([boxMullerRandom()])
            }), 100);
    }

    // handleData(data) {
    //     console.log(data);
    // }

    render() {
        return (
        // <Event event="eventName" handler={this.handleData} />
        <Sparklines data={this.state.data} limit={20}>
            <SparklinesLine color="#1c8cdc" style={{ fill: "none" }} />
            <SparklinesSpots />
        </Sparklines>
        );
    }
}

export function FrequencyChart() {

 

  return (
    <div>
      <Dynamic />
    </div>
  );
}

export default FrequencyChart;
