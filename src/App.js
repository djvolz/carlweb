import React, { Component } from 'react';
import logo from './logo.svg';
import Request from 'react-http-request';

import FrequencyChart from './FrequencyChart.js';
import './App.css';


class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to Corl</h2>
        </div>
        <FrequencyChart />
        
      <Request
        url='http://localhost:8752'
        method='get'
        accept='application/json'
        verbose={true}
      >
        {
          ({error, result, loading}) => {
            if (loading) {
              return <div>loading...</div>;
            } else {
              return <div>{ JSON.stringify(result) }</div>;
            }
          }
        }
      </Request>
      </div>
    );
  }
}

export default App;
